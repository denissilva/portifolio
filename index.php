<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Denis Silva - Programador Fullstack</title>
    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/reset.css?v=<?php echo mt_rand(); ?>">
    <link rel="stylesheet" href="css/index.css?v=<?php echo mt_rand(); ?>">

    <link rel="icon" href="img/favicon.ico" type="image/x-icon">   
</head>
<body>
    <div class="conteudo">
        <nav class="menu">
            <div class="menu-cabecalho">
                <img src="img/foto.png" class="menu-foto">
                <header>
                    <h1>denis.silva</h1>
                    <div class="menu-icone-mobile" onclick="funcaoMenu(this)">
                        <div class="menu-barra"></div>
                        <div class="menu-barra"></div>
                        <div class="menu-barra"></div>
                    </div>
                </header>
                
                <div class="menu-titulos">
                    <hr>
                    <h4>programador fullstack</h4>
                </div>

                <hr>

                <div class="menu-informacoes">
                    <span data-title="Github">
                        <a href="https://github.com/denis1206" target="_blank" >
                            <i class="fab fa-github"></i>
                        </a>
                    </span>
                    <span data-title="Bitbucket">
                        <a href="https://bitbucket.org/denissilva/" target="_blank" id="bitbucket">
                            <span>Bitbucket</span>
                            <i class="fab fa-bitbucket"></i>
                        </a>
                    </span>
                    <span data-title="E-mail">
                        <a href="mailto:denis1206@hotmail.com" target="_blank">
                            <i class="fas fa-envelope"></i>
                        </a>
                    </span>
                </div>
            </div>
            <ul id="menu" class="menu-nav">
                <li><a href="#sobre" class="ativo">sobre</a></li>
                <li><a href="#portifolio">portifólio</a></li>
            </ul>
            <div class="menu-direitos">
                copyright © 2021
            </div>
        </nav>
        
        <main class="principal">
            <section class="sobre" id="sobre">
                <h1>Sobre</h1>

                <div class="sobre-descricao">
                    <p>Olá, meu nome é Dênis Silva. Sou desenvolvedor Fullstack com foco em criação de aplicações web.</p>
                    <p>Procuro sempre me manter atualizado sobre as tecnologias emergentes no mercado, buscando assim, integrar novos conhecimentos com as experiências obtidas durante os 10 anos de atuação na área.</p>
                    <p>Veja mais sobre meu perfil profissional acessando o meu <a href="pdf/Denis Pereira e Silva.pdf?v=<?php echo mt_rand(); ?>" target="_blank">CV</a>, e também minhas participações em artigos disponíveis no meu <a href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K8389920Z2" target="_blank">currículo lattes</a></a>.</p>
                </p>

                <h1>Habilidades</h1>

                <ul>
                    <li>
                        <span>
                            <img src="img/logo/html.png" alt="">
                        </span>
                        <label>HTML5</label>
                    </li>
                    <li>
                        <span>
                            <img src="img/logo/css.png" alt="">
                        </span>
                        <label>CSS3</label>
                    </li>
                    <li>
                        <span>
                            <img src="img/logo/js.png" alt="">
                        </span>
                        <label>JavaScript</label>
                    </li>
                    <li>
                        <span>
                            <img src="img/logo/jq.png" alt="">
                        </span>
                        <label>jQuery</label>
                    </li>
                    <li>
                        <span>
                            <img src="img/logo/ol.png" alt="">
                        </span>
                        <label>OpenLayers</label>
                    </li>
                    <li>
                        <span>
                            <img src="img/logo/gm.png" alt="">
                        </span>
                        <label>Google Maps</label>
                    </li>
                    <li>
                        <span>
                            <img src="img/logo/vue.png" alt="">
                        </span>
                        <label>vue.js</label>
                    </li>
                    <li>
                        <span>
                            <img src="img/logo/angular.png" alt="">
                        </span>
                        <label>Angular</label>
                    </li>
                    <li>
                        <span>
                            <img src="img/logo/hk.png" alt="">
                        </span>
                        <label>Heroku</label>
                    </li>
                    
                    <li>
                        <span>
                            <img src="img/logo/java.png" alt="">
                        </span>
                        <label>Java</label>
                    </li>

                    <li>
                        <span>
                            <img src="img/logo/php.png" alt="">
                        </span>
                        <label>PHP</label>
                    </li>

                    <li>
                        <span>
                            <img src="img/logo/laravel.png" alt="">
                        </span>
                        <label>Laravel</label>
                    </li>
                    

                    <li>
                        <span>
                            <img src="img/logo/wp.png" alt="">
                        </span>
                        <label>Wordpress</label>
                    </li>

                    <li>
                        <span>
                            <img src="img/logo/woocommerce.png" alt="">
                        </span>
                        <label>WooCommerce</label>
                    </li>

                    <li>
                        <span>
                            <img src="img/logo/magento.png" alt="">
                        </span>
                        <label>Magento</label>
                    </li>

                    <li>
                        <span>
                            <img src="img/logo/vtex.png" alt="">
                        </span>
                        <label>VTEX</label>
                    </li>

                    <li>
                        <span>
                            <img src="img/logo/ps.png" alt="">
                        </span>
                        <label>Photoshop</label>
                    </li>

                    <li>
                        <span>
                            <img src="img/logo/adobe_xd.png" alt="">
                        </span>
                        <label>Adobe XD</label>
                    </li>

                    <li>
                        <span>
                            <img src="img/logo/zeplin.png" alt="">
                        </span>
                        <label>Zepplin</label>
                    </li>

                    <li>
                        <span>
                            <img src="img/logo/figma.png" alt="">
                        </span>
                        <label>Figma</label>
                    </li>
                </ul>
            </section>

            <section class="portifolio" id="portifolio">
                <h1>Projetos</h1>
                
                <div class="portifolio-conteudo">
                    <div class="projeto tuia">
                        <div class="projeto-descricao">
                            <h3>Tuia</h3>
                            <a href="https://www.tuiaseguros.com.br/" target="_blank" class="botao">Visualizar</a>

                            <div class="multiple-owners">
                                <p>AidCollective</p>
                            </div>
                        </div>
                    </div>

                    <div class="projeto aurinova">
                        <div class="projeto-descricao">
                            <h3>Aurinova</h3>
                            <a href="https://aurinova.com.br/" target="_blank" class="botao">Visualizar</a>

                            <div class="multiple-owners">
                                <p>AidCollective</p>
                            </div>
                        </div>
                    </div>

                    <div class="projeto cardpay">
                        <div class="projeto-descricao">
                            <h3>CardPay</h3>
                            <a href="https://cardpay.com.br/" target="_blank" class="botao">Visualizar</a>

                            <div class="multiple-owners">
                                <p>Agência: ILI</p>
                                <p>Designer: Gustavo Souza</p>
                                <p>Redação: Gabi Sandoval</p>
                            </div>
                        </div>
                    </div>

                    <div class="projeto grupo_card">
                        <div class="projeto-descricao">
                            <h3>Grupo Card</h3>
                            <a href="https://grupocard.com.br/" target="_blank" class="botao">Visualizar</a>

                            <div class="multiple-owners">
                                <p>Agência: ILI</p>
                                <p>Designer: Gustavo Souza</p>
                            </div>
                        </div>
                    </div>

                    <div class="projeto capital">
                        <div class="projeto-descricao">
                            <h3>3Capital Partners</h3>
                            <a href="http://3capitalpartners.com.br/" target="_blank" class="botao">Visualizar</a>

                            <div class="multiple-owners">
                                <p>Agência: ILI</p>
                                <p>Designer: Gilmarques de Castro</p>
                                <p>Redação: Gabi Sandoval</p>
                            </div>
                        </div>
                    </div>

                    <div class="projeto vitaderm_ecommerce">
                        <div class="projeto-descricao">
                            <h3>VITA DERM E-COMMERCE</h3>
                            <a href="https://www.vitadermstore.com.br/" target="_blank" class="botao">Visualizar</a>

                            <div class="multiple-owners">
                                <p>Agência: ILI</p>
                                <p>Designer: Gilmarques de Castro</p>
                            </div>
                        </div>
                    </div>

                    <div class="projeto aid_collective">
                        <div class="projeto-descricao">
                            <h3>AidCollective</h3>
                            <a href="http://renanemigdio.com/aid/" target="_blank" class="botao">Visualizar</a>

                            <!-- <div class="multiple-owners">
                                <p>Agência: ILI</p>
                                <p>Designer: Gilmarques de Castro</p>
                                <p>Redação: Gabi Sandoval</p>
                            </div> -->
                        </div>
                    </div>

                    <div class="projeto plap">
                        <div class="projeto-descricao">
                            <h3>PLAP</h3>
                            <a href="https://useplap.com.br/" target="_blank" class="botao">Visualizar</a>
                        </div>
                    </div>

                    <div class="projeto mymedi">
                        <div class="projeto-descricao">
                            <h3>MYMEDI</h3>
                            <a href="https://mymedi.com.br" target="_blank" class="botao">Visualizar</a>
                        </div>
                    </div>

                    <div class="projeto mapsat">
                        <div class="projeto-descricao">
                            <h3>MapSAT</h3>
                            <a href="http://satelite.cptec.inpe.br/mapsat/" target="_blank" class="botao">Visualizar</a>
                        </div>
                    </div>
                    <div class="projeto cgpdi">
                        <div class="projeto-descricao">
                            <h3>CGPDI</h3>
                            <a href="http://cgpdi.org.br" target="_blank" class="botao">Visualizar</a>
                        </div>
                    </div>
                    <div class="projeto soschuva">
                        <div class="projeto-descricao">
                            <h3>SOSCHUVA</h3>
                            <a href="http://satelite.cptec.inpe.br/soschuvaapp/" target="_blank" class="botao">Visualizar</a>
                        </div>
                    </div>
                    <div class="projeto raios">
                        <div class="projeto-descricao">
                            <h3>Raios</h3>
                            <a href="http://sigma.cptec.inpe.br/raio/" target="_blank" class="botao">Visualizar</a>
                        </div>
                    </div>
                    <div class="projeto satelite">
                        <div class="projeto-descricao">
                            <h3>DSA/CPTEC</h3>
                            <a href="http://satelite.cptec.inpe.br" target="_blank" class="botao">Visualizar</a>
                        </div>
                    </div>
                    <!-- <div class="projeto gasolution">
                        <div class="projeto-descricao">
                            <h3>Gasolution</h3>
                            <a href="https://gasolution.herokuapp.com/" target="_blank" class="botao">Visualizar</a>
                        </div>
                    </div> -->
                </div>
            </section>
        </main>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script>
        function funcaoMenu(x) {
            document.querySelector("nav").classList.toggle("menu-aberto");
        }

        $(document).ready(function () {
            $('a[href^="#"]').bind("click", function(e) {
                e.preventDefault();

                var headerHeight = $(".menu").height() + 20;
                var target = $(this).attr("href");
                var scrollToPosition = $(target).offset().top;
                var tela = $(window).width();

                if(tela <= '767')
                    scrollToPosition -= headerHeight
                
                $('a').each(function () {
                    $(this).removeClass('ativo');
                })
                $(this).addClass('ativo');
                
                $('.principal').animate({ 'scrollTop': scrollToPosition });
            });
        });
    </script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-W32H6M6');</script>
    <!-- End Google Tag Manager -->

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W32H6M6"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
</body>
</html>